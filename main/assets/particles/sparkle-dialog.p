stars
- Delay -
active: true
lowMin: 0.0
lowMax: 0.0
- Duration - 
lowMin: 1500.0
lowMax: 1500.0
- Count - 
min: 0
max: 1000
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 150.0
relative: true
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.26027396
timeline2: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: true
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 60.0
highMax: 60.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 60.0
highMax: 60.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 4.0
highMax: 16.0
relative: false
scalingCount: 6
scaling0: 0.0
scaling1: 1.0
scaling2: 0.33333334
scaling3: 1.0
scaling4: 0.4509804
scaling5: 1.0
timelineCount: 6
timeline0: 0.0
timeline1: 0.12328767
timeline2: 0.28767124
timeline3: 0.4041096
timeline4: 0.5753425
timeline5: 0.70547944
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 220.0
relative: true
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 1.0
lowMax: 360.0
highMin: -180.0
highMax: 180.0
relative: true
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.51369864
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 12
colors0: 1.0
colors1: 0.99607843
colors2: 0.6784314
colors3: 1.0
colors4: 0.90588236
colors5: 0.0
colors6: 1.0
colors7: 0.5411765
colors8: 0.0
colors9: 1.0
colors10: 0.0
colors11: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.3018322
timeline2: 0.63705105
timeline3: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.34246576
timeline2: 1.0
- Options - 
attached: true
continuous: true
aligned: false
additive: false
behind: false
- Image Path -
/Users/pplante/Desktop/star.png
